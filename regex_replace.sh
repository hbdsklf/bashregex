#!/bin/bash

# sort out any "options"
args=()
for arg in "$@"; do
    case "$arg" in
        --*)
            ;;
        -*)
            ;;
        *)
            args+=("$arg")
    esac
done

if [[ ${#args[*]} -lt 3 ]]; then
    echo './regex_replace.sh <haystack> <regex> <replacement>
Matches multi-line and replaces all occurences. If there is a capture group
in <regex>, only the capture group will be replaced.
Return code is 0 if there was at least one match, 1 otherwise

Example:
./regex_replace.sh "alicefooasdfbarbob" "foo(asdf)bar" "qwer"
will return "alicefooqwerbarbob"'
    exit
fi

# if there are no brackets in <regex>, add them
regexp="${args[1]}"
REGEX=".*\\(.*\\).*"
if ! [[ "$regexp" =~ $REGEX ]]; then
    regexp="($regexp)"
fi

HAYSTACK="${args[0]}"
REPLACEMENT="${args[2]}"
# turn "foo(asdf)bar" into "((foo)asdf(bar))"
REGEX="(("
for (( i=0; i<${#regexp}; i++ )); do
    CHAR="${regexp:$i:1}"
    case "$CHAR" in
        ")")
            CHAR="("
            ;;
        "(")
            CHAR=")"
            ;;
    esac 
    REGEX="$REGEX$CHAR"
done
REGEX="$REGEX))"

# match and put together
[[ "$HAYSTACK" =~ $REGEX ]]
REPLACE="${BASH_REMATCH[2]}${REPLACEMENT}${BASH_REMATCH[3]}"
REPLACED_HAYSTACK="${HAYSTACK//"${BASH_REMATCH[0]}"/"$REPLACE"}"
echo "$REPLACED_HAYSTACK"
[[ "$HAYSTACK" != "$REPLACED_HAYSTACK" ]]
exit $?

